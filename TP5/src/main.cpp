#include "librairies.hpp"
#include "octree.hpp"
#include "BBcolored.hpp"

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}

	Polyhedron mesh;
	std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty())
	{
		std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
		return 1;
	}

	unsigned int nbVerts = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i)
	{
		++nbVerts;
	}
	std::cout << "Nombre de sommets: " << nbVerts << std::endl;

	unsigned int nbEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i)
	{
		++nbEdges;
	}
	nbEdges /= 2;
	std::cout << "Nombre d'arêtes: " << nbEdges << std::endl;

	unsigned int nbFaces = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		++nbFaces;
	}
	std::cout << "Nombre de faces: " << nbFaces << std::endl;


/******************************************************************************************************
 * Tests
********************************************************************************************************/

	OctreeNode root = generateOctree(mesh, MAX_POINT, MAX_DEPTH);

	// OFF file
    std::vector<std::string> FileContent;
    const std::string filename = "mesh.off";
    const std::string filenameOctree = "mesh_octree.off";
    std::ofstream file(filename.c_str());
    if (file.is_open())
    {
        // mode : enum  Mode { ASCII = 0, BINARY, PRETTY }
        CGAL::set_mode(file, CGAL::IO::ASCII);
        file << mesh;
        file.close();
    }
    else throw FileNotOpenError();

    // FileContent = read_file(filename);
	// color_vertices(mesh, root, filename, FileContent);
	draw_bounding_boxes(mesh, root, filenameOctree);

	return 0;
}