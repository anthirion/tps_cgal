#ifndef __BBCOLORED_HPP__
#define __BBCOLORED_HPP__

#include "librairies.hpp"

/**********************************************************************
 * Exception when file opening failed
****************************************************************************/ 

class FileNotOpenError : public std::exception
{
    public:
        const char * what() const noexcept override { return "File has not been opened\n"; }
};

/**********************************************************************
 * Signatures
****************************************************************************/ 

std::vector<std::string> read_file(const std::string);
void color_vertices(Polyhedron&, OctreeNode&, const std::string, const std::vector<std::string>);
void draw_bounding_boxes(Polyhedron&, OctreeNode&, const std::string);

#endif