#include "octree.hpp"

// utility function
int dichotomicSearch(const OctreeNode &node, Vertex_handle &vert)
{
    Point3 center = node.box.center;
    std::vector<int> vectorX, vectorY, vectorZ;
    std::vector<int> aux, v_intersection;
    int index = 0;
    double x = vert->point().x(), y = vert->point().y(), z = vert->point().z();

    if (x < center.x())     vectorX.insert(vectorX.begin(), {1, 2, 3, 4});
    else                    vectorX.insert(vectorX.begin(), {5, 6, 7, 8});
    
    if (y < center.y())     vectorY.insert(vectorY.begin(), {1, 2, 5, 6});
    else                    vectorY.insert(vectorY.begin(), {3, 4, 7, 8});
    
    if (z < center.z())     vectorZ.insert(vectorZ.begin(), {1, 3, 5, 7});
    else                    vectorZ.insert(vectorZ.begin(), {2, 4, 6, 8});
    
    std::set_intersection(vectorX.begin(), vectorX.end(),
                            vectorY.begin(), vectorY.end(),
                            std::back_inserter(aux));
    std::set_intersection(vectorZ.begin(), vectorZ.end(),
                            aux.begin(), aux.end(),
                            std::back_inserter(v_intersection));
    
    if (v_intersection.size() == 1)     index = v_intersection.front();                                
    else                                std::cout << "ERROR DICHOTOMY" << std::endl;

    return index-1;
}


/// @brief Compute the bounding box of a mesh
/// @param mesh the mesh of interest
/// @return its bounding box
AABB computeBB(const Polyhedron &mesh)
{
	double minX = mesh.vertices_begin()->point().x();
	double maxX = mesh.vertices_begin()->point().x();
	double minY = mesh.vertices_begin()->point().y();
	double maxY = mesh.vertices_begin()->point().y();
	double minZ = mesh.vertices_begin()->point().z();
	double maxZ = mesh.vertices_begin()->point().z();

	for (Vertex_const_iterator vert = mesh.vertices_begin(); vert != mesh.vertices_end(); ++vert){
		minX = std::min(minX, vert->point().x());
		minY = std::min(minY, vert->point().y());
		minZ = std::min(minZ, vert->point().z());

		maxX = std::max(maxX, vert->point().x());
		maxY = std::max(maxY, vert->point().y());
		maxZ = std::max(maxZ, vert->point().z());
	}

	AABB box;
	box.center = Point3((minX+maxX)/2, (minY+maxY)/2,(minZ+maxZ)/2);
	box.length = std::max(std::max(maxX-minX, maxY-minY), maxZ-minZ);
	return box;
}


/// @brief add a level to the given parent Octree node, by creating 8 children with 8 bounding box,
/// sliced in the middle of the parent node
/// @param node the octree node to which 8 children will be added
void addOctreeLevel(OctreeNode &node)
{
    double x = node.box.center.x();            
    double y = node.box.center.y();
    double z = node.box.center.z();

    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x-node.box.length/4, y-node.box.length/4, z-node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x-node.box.length/4, y-node.box.length/4, z+node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x-node.box.length/4, y+node.box.length/4, z-node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x-node.box.length/4, y+node.box.length/4, z+node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x+node.box.length/4, y-node.box.length/4, z-node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x+node.box.length/4, y-node.box.length/4, z+node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x+node.box.length/4, y+node.box.length/4, z-node.box.length/4);
        return newNode;
    }());
    node.children.push_back([node,x,y,z] () {
        OctreeNode newNode;
        newNode.box.center = Point3(x+node.box.length/4, y+node.box.length/4, z+node.box.length/4);
        return newNode;
    }());

    for (OctreeNode &n : node.children){
        n.box.length = node.box.length/2;
        n.box.vertices = {};
        n.children = {};
        n.depth = node.depth+1;
    }

    // distribute vertices in children
    for (auto &vert : node.box.vertices)
    {
        int index = dichotomicSearch(node, vert);
        try
        {
            OctreeNode* newNode = &(node.children[index]);
            if (newNode == &node)  std::cerr << "ERROR: adresses are the same" << std::endl;
            // it is impossible that there is too many vertices (more than MAX_POINT)
            // because the parent node do not have more vertices than MAX_POINT
            newNode->box.vertices.push_back(vert);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            std::cerr << "index value: " << index << std::endl;
        }
    }

    // delete all vertices in node
    node.box.vertices.clear();
}

/// @brief add one vertex to an octree, by following strictly the rules of maximum amount of point in a node, and maximum depth of the tree
/// @param root the root node of the tree
/// @param vert the vertex that will be added, as a Vertex_handle
void addVertexToOctree(OctreeNode &root, Vertex_handle &vert, const int maxPts, const int maxDepth)
{
	OctreeNode* currentNode = &root;

    double x, y, z; 
    int index = 0;
    // double childLength;
    // Point3 childCenter;
    
    x = vert->point().x();
    y = vert->point().y();
    z = vert->point().z();
    while (currentNode->children.size() > 0){
        // currentNode is not a leaf

        // oui mais on peut optimiser -> dichotomie sur trois axes!
        // for (OctreeNode child : currentNode.children){

        //     childCenter = child.box.center;
        //     childLength = child.box.length;
        //     if (x < childCenter.x()+childLength/2 && x > childCenter.x()-childLength/2 &&
        //         y < childCenter.y()+childLength/2 && y > childCenter.y()-childLength/2 &&
        //         z < childCenter.z()+childLength/2 && z > childCenter.z()-childLength/2){
        //             currentNode = child;
        //         }
        // }

        // optimisation par dichotomie
        index = dichotomicSearch(*currentNode, vert);
        try
        {
            currentNode = &(currentNode->children[index]);
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
            std::cerr << "index value: " << index << std::endl;
        }        
    }

    if (currentNode->depth == maxDepth || currentNode->box.vertices.size() < maxPts)
    {
        currentNode->box.vertices.push_back(vert);
    }
    else
    {
        addOctreeLevel(*currentNode);
        index = dichotomicSearch(*currentNode, vert);
        try
        {
            currentNode->children[index].box.vertices.push_back(vert);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            std::cerr << "index value: " << index << std::endl;
        }
    }

}

/// @brief A function to generate an octree of the vertices of a mesh,
/// Each vertex will be stored in a node of the octree.
/// the octree shall follow two rules:
///    1- each node shall only contain MAX_POINT vertices
///    2- the depth of tree shall not exceed MAX_DEPTH
///	Remark: the depth of the root is 0
///	Remark: rule 2 wins over rule 1.
/// i.e. a node may contain more vertices than MAX_POINT if the maximum depth is reached.
/// @param mesh the mesh of interest
/// @return an octree node that is the root of the octree for the given mesh
OctreeNode generateOctree(Polyhedron &mesh, const int maxPts, const int maxDepth)   
{
	OctreeNode root{};

    // initialization of root box
    root.box = computeBB(mesh);
    
    for (Vertex_iterator vert_it = mesh.vertices_begin(); vert_it != mesh.vertices_end(); ++vert_it){
        addVertexToOctree(root, vert_it, maxPts, maxDepth);
    }

	return root;
}



/// @brief find a specific vertex inside an octree (using a dichotomy algorithm)
/// @param vh the vertex handle to look for
/// @return the address of the node (not the prettiest way, feel free to handle it differently)

OctreeNode* findVertexInOctree(OctreeNode& root, Vertex_handle &vert)
{
	OctreeNode* node = &root;

    while (node->children.size() > 0)
	{
        int index = dichotomicSearch(*node, vert);
        try
        {
            node = &(node->children[index]);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            std::cerr << "index value: " << index << std::endl;
        }
    }
	return node;
}

/// @brief (optional) Utility function that takes an octree and apply a function (or more useful, a lambda !)
/// to each leaf of the Octree (each node containing vertices).
/// Can be useful to avoid declaring a new recursive function each time...
/// @param root the root node of the Octree of interest
/// @param func a lambda supposed to do something on a given Octree node.
void browseNodes(const OctreeNode &root, std::function<void(const OctreeNode&)> func)
{
    if (root.box.vertices.size() > 0){
        func(root);
    }
    for (auto child : root.children)
        browseNodes(child, func);
}


void extractMeshFromOctree(const OctreeNode &root, const Polyhedron& mesh){

	std::vector<Point3> vertices;
	std::vector<std::vector<int>> faces;

	// TODO: fill "vertices" and "faces" by going through the octree

	std::ofstream out("octree_meshres.off");
	out << "OFF" << std::endl;
	out << vertices.size() << " " << faces.size() << " 0" << std::endl;
	for (const auto &v : vertices)
	{
		out << v.x() << " " << v.y() << " " << v.z() << std::endl;
	}
	for (const auto &f : faces)
	{
		out << f.size() << " ";
		for(auto fi : f){
			out << fi << " ";
		}
		out << std::endl;
	}

}

/// @brief function that takes an octree node and merge all the vertices it contains in one
/// the new vertex will be the barycenter of the merged vertices
/// @param node the node from which the vertices are merged
OctreeNode mergeVerticesInNode (const OctreeNode & node){

    OctreeNode newNode = node;

    Polyhedron::Vertex_handle newVertex = barrycenter(node.box.vertices);

    std::vector<Polyhedron::Vertex_handle> newVector {newVertex};

    newNode.box.vertices = newVector;

    return newNode;
}

Polyhedron::Vertex_handle barrycenter(const std::vector<Polyhedron::Vertex_handle>& verts){
    
    int n = verts.size();
    //Si la doc l'explique clairement quelque part, initialiser proprement le pt à 0,0,0
    double x = 0;
    double y = 0;
    double z = 0;

    for (auto v : verts){
        x += v->point().x();
        y += v->point().y();
        z += v->point().z();
    }

    x /= n;
    y /= n;
    z /= n;
    
    Point3 barrycenter = Point3(x, y, z);
    Polyhedron::Vertex_handle res;
    res->point() = barrycenter;

    return res;
}
