// implémentation de la première méthode
#include "BBcolored.hpp"
#include "octree.hpp"

std::vector<std::string> read_file(const std::string filename)
{
	std::vector<std::string> FileContent;
	std::string line;
	std::ifstream file(filename.c_str());

	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, line);
			FileContent.push_back(line);
		}
		file.close();
	}
	else throw FileNotOpenError();
	return FileContent;
}



// save the polyhedron in the off format with colored vertices
void color_vertices(Polyhedron &mesh, OctreeNode& root, const std::string filename, const std::vector<std::string> FileContent)
{
    std::ofstream file(filename.c_str());
    int nb_vertices, nb_faces;
    int nb_line = 3, n_color = 0;
    double numerator = 1.0, denominator = 1.0, value = numerator/denominator;


	if (file.is_open())
	{
        file << "COFF" << std::endl;
        file << FileContent[1] << std::endl;
        file << std::endl;

		std::stringstream ss;
		ss << FileContent[1];
		ss >> nb_vertices >> nb_faces;


        // Change the off file
        // Change the lines corresponding to the vertices
        std::map<OctreeNode*, std::array<float, 3>> node_colors;
        std::array<float, 3> color = {0, 0, 0};

        for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i)
	    {
            // determiner a quelle node le sommet appartient
            OctreeNode* node = findVertexInOctree(root, i);
            auto search = node_colors.find(node);
            if (search != node_colors.end())
            {
                color = search->second;
            }
            else
            {
                color[n_color] = value;
                node_colors.insert(std::make_pair(node, color));
                if (n_color == 2)
                {
                    n_color = 0;
                    // changer valeur
                    if (value < 1)  numerator += 2.0;
                    else
                    {
                        denominator *= 2.0;
                        numerator = 1.0;
                    }
                    value = numerator/denominator;
                }
                else    ++n_color;
            }
			file << FileContent[nb_line] << " " << std::to_string(color[0]) << " "
                                                << std::to_string(color[1]) << " "
                                                << std::to_string(color[2]) << " "
                                                << std::endl;
            ++nb_line;
		}

		// Do not modify the last lines corresponding to the faces
		for (nb_line = nb_vertices+3; nb_line <= nb_vertices+nb_faces+3; nb_line++)
		{
			file << FileContent[nb_line] << std::endl;
		}

		file.close();
	}
	else throw FileNotOpenError();
}


void draw_bounding_boxes(Polyhedron &mesh, OctreeNode& root, const std::string filenameOctree)
{
    std::ofstream fileOctree(filenameOctree.c_str());
    std::set<OctreeNode*> nodes;
    // count the number of boxes to draw
    int cpt = 0;


    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i)
    {
        // determiner a quelle node le sommet appartient
        OctreeNode* node = findVertexInOctree(root, i);
        auto search = nodes.find(node);
        if (search == nodes.end())
        {
            // node not found
            nodes.insert(node);
            cpt++;
        }
    }

    if (fileOctree.is_open())
    {
        int deb = 0;

        fileOctree << "OFF" << std::endl;
        fileOctree << std::to_string(cpt*8) << " "
                << std::to_string(cpt*6) << " "
                << "0" << std::endl;
        fileOctree << std::endl;


        // Add the vertices of the bounding boxes
        for (OctreeNode* node: nodes)
        {      
            // 8 vertices of the box
            fileOctree << std::to_string(node->box.center.x()) << " "
                    << std::to_string(node->box.center.y() - node->box.length/2.0) << " "
                    << std::to_string(node->box.center.z() - node->box.length/2.0) << std::endl;
            
            fileOctree << std::to_string(node->box.center.x() - node->box.length/2.0) << " "
                    << std::to_string(node->box.center.y()) << " "
                    << std::to_string(node->box.center.z() - node->box.length/2.0) << std::endl;

            fileOctree << std::to_string(node->box.center.x()) << " "
                    << std::to_string(node->box.center.y() + node->box.length/2.0) << " "
                    << std::to_string(node->box.center.z() - node->box.length/2.0) << std::endl;
            
            fileOctree << std::to_string(node->box.center.x() + node->box.length/2.0) << " "
                    << std::to_string(node->box.center.y()) << " "
                    << std::to_string(node->box.center.z() - node->box.length/2.0) << std::endl;
            
            
            fileOctree << std::to_string(node->box.center.x()) << " "
                    << std::to_string(node->box.center.y() - node->box.length/2.0) << " "
                    << std::to_string(node->box.center.z() + node->box.length/2.0) << std::endl;
            
            fileOctree << std::to_string(node->box.center.x() - node->box.length/2.0) << " "
                    << std::to_string(node->box.center.y()) << " "
                    << std::to_string(node->box.center.z() + node->box.length/2.0) << std::endl;

            fileOctree << std::to_string(node->box.center.x()) << " "
                    << std::to_string(node->box.center.y() + node->box.length/2.0) << " "
                    << std::to_string(node->box.center.z() + node->box.length/2.0) << std::endl;
            
            fileOctree << std::to_string(node->box.center.x() + node->box.length/2.0) << " "
                    << std::to_string(node->box.center.y()) << " "
                    << std::to_string(node->box.center.z() + node->box.length/2.0) << std::endl;
                
        }


        // Add the faces of the bounding boxes
        for (int k = 0; k < cpt; k++)
        {
            deb = k*8;
            fileOctree << "4 " 
                    << std::to_string(deb+0) << " "
                    << std::to_string(deb+1) << " "
                    << std::to_string(deb+2) << " "
                    << std::to_string(deb+3) << std::endl;

            fileOctree << "4 " 
                    << std::to_string(deb+4) << " "
                    << std::to_string(deb+5) << " "
                    << std::to_string(deb+6) << " "
                    << std::to_string(deb+7) << std::endl;
            
            fileOctree << "4 " 
                    << std::to_string(deb+0) << " "
                    << std::to_string(deb+4) << " "
                    << std::to_string(deb+5) << " "
                    << std::to_string(deb+1) << std::endl;

            fileOctree << "4 " 
                    << std::to_string(deb+1) << " "
                    << std::to_string(deb+5) << " "
                    << std::to_string(deb+6) << " "
                    << std::to_string(deb+2) << std::endl;

            fileOctree << "4 " 
                    << std::to_string(deb+6) << " "
                    << std::to_string(deb+2) << " "
                    << std::to_string(deb+3) << " "
                    << std::to_string(deb+7) << std::endl;
            
            fileOctree << "4 " 
                    << std::to_string(deb+3) << " "
                    << std::to_string(deb+7) << " "
                    << std::to_string(deb+4) << " "
                    << std::to_string(deb+0) << std::endl;
        }
        fileOctree.close();
    }
    else throw FileNotOpenError();
}