#ifndef __OCTREE_ANTJAR__
#define __OCTREE_ANTJAR__

#include "librairies.hpp"

#define MAX_POINT 35 // for testing purposes, 
#define MAX_DEPTH 10 // it would be much better if these values were given to the function where the tree is being constructed.

int dichotomicSearch(const OctreeNode &, Vertex_handle &);

AABB computeBB(const Polyhedron &);

void addOctreeLevel(OctreeNode &);
void addVertexToOctree(OctreeNode &, Vertex_handle &, const int, const int);

OctreeNode generateOctree(Polyhedron &, const int, const int);
OctreeNode* findVertexInOctree(OctreeNode&, Vertex_handle &);

void browseNodes(const OctreeNode &, std::function<void(const OctreeNode&)> );
void extractMeshFromOctree(const OctreeNode &, const Polyhedron& );

Polyhedron::Vertex_handle barrycenter(const std::vector<Polyhedron::Vertex_handle>&);
OctreeNode mergeVerticesInNode (const OctreeNode &);

#endif
