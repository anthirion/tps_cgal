#ifndef __LIBRAIRIES__THIJAR__
#define __LIBRAIRIES__THIJAR__

#include <CGAL/Polyhedron_3.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>

#include <vector>
#include <algorithm>

#include <typeinfo> //DEBUG PURPOSE

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Vertex_const_iterator Vertex_const_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Vertex_handle Vertex_handle;
typedef Polyhedron::Vertex_const_handle Vertex_const_handle;

typedef Polyhedron::Point_3 Point3;

/// @brief Axis-Aligned bounding box
struct AABB
{
	Point3 center; //Center of the box
	double length; //Size of the box
	std::vector<Polyhedron::Vertex_handle> vertices; //vertices contained in the box
};

struct OctreeNode
{
	std::vector<OctreeNode> children;
	AABB box;
    int depth; // utilité ?
};

#endif