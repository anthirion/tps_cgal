#ifndef __FILEMANAGEMENT_HPP__
#define __FILEMANAGEMENT_HPP__

#include "libraries.hpp"

/**********************************************************************
 * Exception when file opening failed
****************************************************************************/ 

class FileNotOpenError : public std::exception
{
    public:
        const char * what() const noexcept override { return "File has not been opened\n"; }
};


/**********************************************************************
 * Signatures
****************************************************************************/ 

std::vector<std::string> read_file(const std::string);
void add_colors(const Facet_int_map&, const std::string, const std::vector<std::string>);

#endif