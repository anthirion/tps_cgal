#ifndef __MEASURES_HPP__
#define __MEASURES_HPP__

#include "libraries.hpp"

std::vector<double> computePerimeters(Polyhedron&);
double getPerimeter(const Polyhedron::Facet_handle &);

CGAL::Angle getAngleX(const Polyhedron::Facet_handle&);
CGAL::Angle getAngleY(const Polyhedron::Facet_handle&);
CGAL::Angle getAngleZ(const Polyhedron::Facet_handle&);
std::vector<CGAL::Angle> computeAnglesX(Polyhedron&);
std::vector<CGAL::Angle> computeAnglesY(Polyhedron&);
std::vector<CGAL::Angle> computeAnglesZ(Polyhedron&);

double getArea(const Polyhedron::Facet_handle&);
std::vector<double> computeAreas(Polyhedron&);

#endif