#ifndef __LIBRARIES_HPP__
#define __LIBRARIES_HPP__

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/enum.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <map>
#include <set>
#include <array>
#include <string>
#include <utility>
#include <algorithm>
#include <cmath>
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef Kernel::Vector_3 Vector3;
typedef std::map<Polyhedron::Facet_handle, double> Facet_double_map;
typedef std::map<Polyhedron::Facet_handle, int> Facet_int_map;
typedef std::set<Polyhedron::Facet_handle> Facet_set;

#endif