#include "measures.hpp"
#include "libraries.hpp"

/**********************************************************************
 * Compute perimeters of each face
****************************************************************************/ 
std::vector<double> computePerimeters(Polyhedron & mesh)
{
	std::vector<double> perimeters;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		perimeters.push_back(getPerimeter(i));
	}
	return perimeters;
}


/**********************************************************************
 * Compute the perimeterof a face
****************************************************************************/ 
double getPerimeter(const Polyhedron::Facet_handle & face){

	Halfedge_facet_circulator j = face->facet_begin();
	double perimeter = 0.0, distance = 0.0;
	do {
		auto p1 = j->vertex()->point();
		j++;
		auto p2 = j->vertex()->point();
		distance = CGAL::squared_distance(p1, p2);
		if (distance > 0)	perimeter += sqrt(distance);
	} while (j != face->facet_begin());
	
	return perimeter;
}


/**********************************************************************
 * Compute the angle between the normal of a face and the x-y-z axis, 
 * for each face
****************************************************************************/ 

CGAL::Angle getAngleX(const Polyhedron::Facet_handle & face)
{
	const Vector3 X_axis(1.0, 0.0, 0.0);

	Halfedge_facet_circulator j = face->facet_begin();
	auto p1 = j->vertex()->point();
	++j;
	auto p2 = j->vertex()->point();
	++j;
	auto p3 = j->vertex()->point();
	auto normal = CGAL::normal(p1, p2, p3);
	return CGAL::angle(X_axis, normal);
}

CGAL::Angle getAngleY(const Polyhedron::Facet_handle & face)
{
	const Vector3 Y_axis(0.0, 1.0, 0.0);

	Halfedge_facet_circulator j = face->facet_begin();
	auto p1 = j->vertex()->point();
	++j;
	auto p2 = j->vertex()->point();
	++j;
	auto p3 = j->vertex()->point();
	auto normal = CGAL::normal(p1, p2, p3);
	return CGAL::angle(Y_axis, normal);
}

CGAL::Angle getAngleZ(const Polyhedron::Facet_handle & face)
{
	const Vector3 Z_axis(0.0, 0.0, 1.0);

	Halfedge_facet_circulator j = face->facet_begin();
	auto p1 = j->vertex()->point();
	++j;
	auto p2 = j->vertex()->point();
	++j;
	auto p3 = j->vertex()->point();
	auto normal = CGAL::normal(p1, p2, p3);
	return CGAL::angle(Z_axis, normal);
}

std::vector<CGAL::Angle> computeAnglesX(Polyhedron & mesh)
{
	std::vector<CGAL::Angle> angles_x;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		angles_x.push_back(getAngleX(i));
	}
	return angles_x;
}

std::vector<CGAL::Angle> computeAnglesY(Polyhedron & mesh)
{
	std::vector<CGAL::Angle> angles_y;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		angles_y.push_back(getAngleY(i));
	}
	return angles_y;
}

std::vector<CGAL::Angle> computeAnglesZ(Polyhedron & mesh)
{
	std::vector<CGAL::Angle> angles_z;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		angles_z.push_back(getAngleZ(i));
	}
	return angles_z;
}

/**********************************************************************
 * Compute the area of all faces
****************************************************************************/ 
std::vector<double> computeAreas(Polyhedron & mesh)
{
	std::vector<double> areas;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		areas.push_back(getArea(i));
	}
	return areas;
}

/**********************************************************************
 * Compute the area of a face
****************************************************************************/ 
double getArea(const Polyhedron::Facet_handle & face){

	Halfedge_facet_circulator j = face->facet_begin();
		double areaface = 0.0, area = 0.0;
		j++;
		auto p1 = j->vertex()->point();
		do {
			++j;
			auto p2 = j->vertex()->point();
            area = CGAL::squared_area(face->facet_begin()->vertex()->point(), p1, p2);
			if (area > 0)	areaface += sqrt(area);
			p1 = p2;
        } while (j != face->facet_begin());
	
	return areaface;
}
