#include "fileManagement.hpp"

std::vector<std::string> read_file(const std::string filename)
{
	std::vector<std::string> FileContent;
	std::string line;
	std::ifstream file(filename.c_str());

	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, line);
			FileContent.push_back(line);
		}
		file.close();
	}
	else throw FileNotOpenError();
	return FileContent;
}




// save the polyhedron in the off format with colored faces
void add_colors(const Facet_int_map& faces_classes, const std::string filename, 
				const std::vector<std::string> FileContent)
{
	std::ofstream file(filename.c_str());
	int nb_vertices, nb_faces;
	int nb_line = 0, n_class = 0, nb_classes = 0;


	if (file.is_open())
	{
		std::stringstream ss;
		ss << FileContent[1];
		ss >> nb_vertices >> nb_faces;

        // get the number of classes
        for (auto p: faces_classes)
        {
            nb_classes = std::max(nb_classes, p.second);
        }
  

        // create a map which matches a class with a color
        std::map<int, std::array<double, 3>> class_colors;
        std::array<double, 3> color = {0.0, 0.0, 0.0};
        int n_color = 0;
        float step;
        class_colors.insert(std::make_pair(n_class, color));
        if (nb_classes != 0)    step = 3.0/nb_classes;
        else                    step = 1.0;

        for (int n_class = 1; n_class < nb_classes+1; n_class++)
        {
            float remainder = step;
            while (remainder > 0)
            {
                try
                {
                    color[n_color] += remainder;
                    if (color[n_color] >= 1)
                    {
                        remainder = color[n_color] - 1;
                        color[n_color] = 1;
                        // passer à la couleur suivante
                        n_color++;
                    }
                    else    remainder = 0;      
                }
                catch (const std::out_of_range &e)
                {
                    e.what();
                }
            }
            class_colors.insert(std::make_pair(n_class, color));
        }
        
        // Write the off file
		// Do not modify the first three lines and the lines corresponding to the vertices
		while (nb_line < nb_vertices+3)
		{
			file << FileContent[nb_line] << std::endl;
			++nb_line;
		}

		// add colors to the lines corresponding to the faces
		for (auto p: faces_classes)
		{
            n_class = p.second;
            std::array<double, 3> color = class_colors[n_class];
			file << FileContent[nb_line] << " " << std::to_string(color[0]) << " "
                                                << std::to_string(color[1]) << " "
                                                << std::to_string(color[2]) << " "
                                                << "1.0" << std::endl;
			++nb_line;
		}
		file.close();
	}
	else throw FileNotOpenError();
}