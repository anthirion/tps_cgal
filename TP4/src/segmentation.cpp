#include "segmentation.hpp"

/**********************************************************************
 * First segmentation
****************************************************************************/ 

double getMean(const Facet_double_map & values)
{
	double sum = 0.0;
	for (auto p: values)
	{
		sum += p.second;
	}
	return sum / values.size();
}

double getMedian(const Facet_double_map & values)
{
	std::vector<double> val;

	for (auto p: values)
	{
		val.push_back(p.second);
	}
	std::sort(val.begin(), val.end());
	int len = val.size();
	if (len % 2 == 0)	return val[len/2];
	else				return val[len/2 + 1];
}

Facet_int_map simpleThreshold(const Facet_double_map & values, const double threshold)
{
	Facet_int_map map_classes;
	int classe;
	for (auto p: values)
	{
		if (p.second < threshold)	classe = 0;
		else	classe = 1;
		map_classes.insert(std::make_pair(p.first, classe));
	}
	return map_classes;
}

/**********************************************************************
 * Second segmentation taking into account connected spaces
****************************************************************************/

Facet_int_map segmentationParCC(Polyhedron &mesh, const Facet_int_map &segmentation)
{
	Facet_int_map CC_map_classes;
	// create a list of visited facet
	// use a set to look for facet in constant time
	Facet_set visited_faces;

	int classe = 0;
	//Visiting every face
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		auto it = visited_faces.find(i);
		if (it == visited_faces.end())
		{
			// face not visited
			VisitCC(i, visited_faces, CC_map_classes, classe, segmentation);
			++classe;
		}
	}
	return CC_map_classes;
}

void VisitCC(const Polyhedron::Facet_handle& face, Facet_set& visited_faces, Facet_int_map& CC_map_classes, int classe, const Facet_int_map &segmentation){
	// face is added to the set of visited faces
	visited_faces.insert(face);

	CC_map_classes.insert(std::make_pair(face, classe));//On attribut une classe à cette face

	//on récupère les faces voisines
	std::vector<Polyhedron::Facet_handle> neighbors = getNeighbors(face);

	for (auto f: neighbors){
		auto it = visited_faces.find(f);
		if((it == visited_faces.end()) && (segmentation.at(f) == segmentation.at(face))){
			VisitCC(f, visited_faces, CC_map_classes, classe, segmentation);
			++classe;
		}
	}
}

std::vector<Polyhedron::Facet_handle> getNeighbors(Polyhedron::Facet_handle face){
	Halfedge_facet_circulator facet_it = face->facet_begin();
	std::vector<Polyhedron::Facet_handle> neighbors;
	do{
		neighbors.push_back(facet_it->opposite()->facet());
	}while (++facet_it != face->facet_begin());

	return neighbors;
}

/**********************************************************************
 * segmentation using a local approach with the perimeters of the triangles
****************************************************************************/

Facet_int_map segmentationLocale(Polyhedron& mesh, const float threshold){

	Facet_int_map Local_segmentation;

	//p.second -> visited tag
	//p.first -> the face
	Facet_set visited_faces;

	int classe = 0;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i){

		auto search = visited_faces.find(i);
		if (search == visited_faces.end()){ //if the face is not included yet in the set
			LocalVisit(i, visited_faces, Local_segmentation, threshold, classe);
			++classe;

		}
	}

	std::cout << classe << " classes have been made" << std::endl;
	return Local_segmentation;
}

void LocalVisit(Polyhedron::Facet_handle& face, Facet_set& visited_faces, Facet_int_map& Local_segmentation, const float threshold, int classe){

	visited_faces.insert(face);
	
	Local_segmentation.insert(std::make_pair(face, classe));
	
	float perimeter = getPerimeter(face);
	std::vector<Polyhedron::Facet_handle> neighbors = getNeighbors(face);

	for (auto f: neighbors){
		
		float neighborPerim = getPerimeter(f);

		auto search = visited_faces.find(f);
		if ((std::abs(perimeter - neighborPerim) < threshold) && (search == visited_faces.end())){ //Perimeter equivalent regarding the threshold value
			LocalVisit(f, visited_faces, Local_segmentation, threshold, classe);
		}

	}
}