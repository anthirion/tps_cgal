#include "libraries.hpp"
#include "measures.hpp"
#include "fileManagement.hpp"
#include "segmentation.hpp"


int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
	std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty()) 
	{
		std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
		return 1;
	}
  
  	unsigned int nbVerts = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		++nbVerts;
	}
	std::cout << "Nombre de sommets: " << nbVerts << std::endl;
	
	unsigned int nbEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i) {
		++nbEdges;
	}
	nbEdges /= 2;
	std::cout << "Nombre d'arêtes: " << nbEdges << std::endl;

	unsigned int nbFaces = 0;

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		++nbFaces;
	}
	std::cout << "Nombre de faces: " << nbFaces << std::endl;
	
	
	unsigned int euler = nbVerts - nbEdges + nbFaces;
	unsigned int genus = (2 - euler) / 2;
	std::cout << "En supposant que le maillage contienne une unique surface sans bord, alors son genre est de " << genus << std::endl;


	/**********************************************************************
	 * Decomment below the measure you want
	****************************************************************************/ 

	// computation of several measures: perimeters, areas and angles
	std::map<Polyhedron::Facet_handle, double> perimeters;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		perimeters.insert(std::make_pair(i, getPerimeter(i)));
	}

	// std::map<Polyhedron::Facet_handle, double> areas;
	// for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
	// 	areas.insert(std::make_pair(i, getArea(i)));
	// }

	// std::map<Polyhedron::Facet_handle, double> anglesX;
	// for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
	// 	anglesX.insert(std::make_pair(i, getAngleX(i)));
	// }


	/**********************************************************************
	 * Save the polyhedron mesh with colors in a file named "test.off"
	****************************************************************************/ 
	
	std::vector<std::string> FileContent;
	const std::string filename = "test.off";
	std::ofstream file(filename.c_str());
	if (file.is_open())
	{
		// mode : enum  Mode { ASCII = 0, BINARY, PRETTY }
		CGAL::set_mode(file, CGAL::IO::ASCII);
		file << mesh;
		file.close();
	}
	else throw FileNotOpenError();
	
	FileContent = read_file(filename);

	// choose the measure you want: perimeters, areas or anglesX
	// decomment above the computation of the chosen measure
	std::map<Polyhedron::Facet_handle, double> measures = perimeters;

	double mean = getMean(measures);
	// double median = getMedian(measures);
	// segment with the mean or the median
	Facet_int_map classes = simpleThreshold(measures, mean);
	// manual threshold
	// Facet_int_map classes = simpleThreshold(measures, 0.5);
	add_colors(classes, filename, FileContent); 


	/**********************************************************************
	 * Save the polyhedron mesh with colors in a file named "test_CC.off"
	****************************************************************************/ 
	
	const std::string filename2 = "test_CC.off";
	std::ofstream file2(filename2.c_str());
	if (file2.is_open())
	{
		// mode : enum  Mode { ASCII = 0, BINARY, PRETTY }
		CGAL::set_mode(file2, CGAL::IO::ASCII);
		file2 << mesh;
		file2.close();
	}
	else throw FileNotOpenError();
	
	FileContent = read_file(filename2);
	Facet_int_map CC_classes = segmentationParCC(mesh, classes);
	add_colors(CC_classes, filename2, FileContent); 

	/**********************************************************************
	 * Save the polyhedron mesh with colors regarding the perimeters in a file named "test_local.off"
	****************************************************************************/ 
	
	const std::string filename3 = "test_local.off";
	std::ofstream file3(filename3.c_str());
	if (file3.is_open())
	{
		// mode : enum  Mode { ASCII = 0, BINARY, PRETTY }
		CGAL::set_mode(file3, CGAL::IO::ASCII);
		file3 << mesh;
		file3.close();
	}
	else throw FileNotOpenError();
	
	FileContent = read_file(filename3);
	Facet_int_map Perimeter_classes = segmentationLocale(mesh, 0.002);
	add_colors(Perimeter_classes, filename3, FileContent); 

	return 0;
}
