#ifndef __SEGMENTATION_HPP__
#define __SEGMENTATION_HPP__

#include "libraries.hpp"
#include "measures.hpp"

double getMean(const Facet_double_map&);
double getMedian(const Facet_double_map&);
Facet_int_map simpleThreshold(const Facet_double_map&, const double);
Facet_int_map segmentationParCC(Polyhedron&, const Facet_int_map&);
Facet_int_map segmentationLocale(Polyhedron&, const float);

void VisitCC(const Polyhedron::Facet_handle&, Facet_set&, Facet_int_map&, int, const Facet_int_map&);
void LocalVisit(Polyhedron::Facet_handle&, Facet_set&, Facet_int_map&, const float, int);

std::vector<Polyhedron::Facet_handle> getNeighbors(Polyhedron::Facet_handle);


#endif